/**
 * @format
 */
import React from 'react';
import {AppRegistry} from 'react-native';
import App from './src/App';
import {GlobalProvider} from './src/context/globalContext';
import {name as appName} from './app.json';
const AppContainer = () => (
  <>
    <GlobalProvider>
      <App />
    </GlobalProvider>
  </>
);
AppRegistry.registerComponent(appName, () => AppContainer);
