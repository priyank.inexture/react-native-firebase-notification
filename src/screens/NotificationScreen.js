import React, {useEffect} from 'react';
import {StyleSheet, View, Text, Dimensions, Image, Button} from 'react-native';
import {
  ADD_NEW_ARTICLE_ACTION,
  UPDATE_NOTIFICATION,
} from '../context/ActionType';
import {
  useGlobalDispatchContext,
  useGlobalStateContext,
} from '../context/globalContext';
const {width, height} = Dimensions.get('window');
function NotificationScreen({navigation}) {
  const {notification} = useGlobalStateContext();
  const dispatch = useGlobalDispatchContext();
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Alert</Text>
      <View style={styles.subContainer}>
        {notification && (
          <>
            <Text style={styles.title}>
              {notification && notification.title}
            </Text>
            <Image
              style={styles.img}
              source={{
                uri: notification && notification.urlToImage,
              }}
            />
            <Button
              title="Explore"
              onPress={() => {
                dispatch({type: UPDATE_NOTIFICATION, notification: false});
                navigation.navigate('Detail', {
                  ...notification,
                });
              }}
            />
          </>
        )}
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: width * 0.1,
  },
  header: {
    textAlign: 'center',
    fontFamily: 'Nunito-SemiBold',
    fontSize: 20,
  },
  subContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontFamily: 'Nunito-Regular',
    textAlign: 'justify',
    marginVertical: height * 0.025,
  },
  img: {
    width: width * 0.5,
    height: width * 0.5,
    marginVertical: height * 0.05,
    resizeMode: 'cover',
  },
});
export default NotificationScreen;
