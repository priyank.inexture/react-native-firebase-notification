import React, {useEffect, useRef} from 'react';
import PushNotification from 'react-native-push-notification';
import {
  StatusBar,
  FlatList,
  Dimensions,
  StyleSheet,
  Animated,
  Text,
  View,
  Button,
} from 'react-native';
import {useGlobalStateContext} from '../context/globalContext';
import Article from '../components/Article';
const {height, width} = Dimensions.get('window');
function HomeScreen({navigation}) {
  const {articles, notification} = useGlobalStateContext();
  const scrollY = useRef(new Animated.Value(0)).current;
  const opacity = scrollY.interpolate({
    inputRange: [-1, 0, height * 0.02],
    outputRange: [1, 1, 0],
  });
  useEffect(() => {
    if (notification) {
      navigation.navigate('Notification');
    }
    // setTimeout(() => {
    //   PushNotification.localNotificationSchedule({
    //     bigText: 'New Article',
    //     subText: 'Your favorite author posted a new article in a while!',
    //     message: 'Your favorite author posted a new article in a while!', // (required)
    //     body: {
    //       source: {
    //         id: 'the-wall-street-journal',
    //         name: 'The Wall Street Journal',
    //       },
    //       author: 'Nina Trentmann',
    //       title:
    //         'Pinterest CFO Looks to Ramp Up Spending on Marketing, Product Investments',
    //       description:
    //         'The image-sharing platform expects the number of U.S. users to plateau as the pandemic abates',
    //       url:
    //         'https://www.wsj.com/articles/pinterest-cfo-looks-to-ramp-up-spending-on-marketing-product-investments-11619570245',
    //       urlToImage: 'https://images.wsj.net/im-330866/social',
    //       publishedAt: '2021-04-28T00:41:49Z',
    //       content:
    //         'Pinterest Inc.s\r\nfinance chief plans to spend more money on marketing to offset a potential slowdown in activity on its image-sharing platform as the U.S. economy reopens.The San Francisco-based comp… [+2861 chars]',
    //     },
    //     date: new Date(Date.now() + 10), // in 60 secs
    //     allowWhileIdle: false, // (optional) set notification to work while on doze, default: false
    //   });
    // }, 1000);
  }, [notification, navigation]);
  return (
    <View>
      {/* <View>
        <Button
          title="send"
          onPress={() => {
            PushNotification.localNotificationSchedule({
              //... You can use all the options from localNotifications
              bigText: 'New Article',
              message: 'My Notification Message', // (required)
              body: JSON.stringify({
                source: {
                  id: 'the-wall-street-journal',
                  name: 'The Wall Street Journal',
                },
                author: 'Nina Trentmann',
                title:
                  'Pinterest CFO Looks to Ramp Up Spending on Marketing, Product Investments',
                description:
                  'The image-sharing platform expects the number of U.S. users to plateau as the pandemic abates',
                url:
                  'https://www.wsj.com/articles/pinterest-cfo-looks-to-ramp-up-spending-on-marketing-product-investments-11619570245',
                urlToImage: 'https://images.wsj.net/im-330866/social',
                publishedAt: '2021-04-28T00:41:49Z',
                content:
                  'Pinterest Inc.s\r\nfinance chief plans to spend more money on marketing to offset a potential slowdown in activity on its image-sharing platform as the U.S. economy reopens.The San Francisco-based comp… [+2861 chars]',
              }),
              date: new Date(Date.now() + 10), // in 60 secs
              allowWhileIdle: false, // (optional) set notification to work while on doze, default: false
            });
          }}
        />
      </View> */}
      <Animated.Text style={[styles.text, {opacity}]}>Articles</Animated.Text>
      <FlatList
        onScroll={Animated.event([
          {
            nativeEvent: {
              contentOffset: {
                y: scrollY,
              },
            },
          },
        ])}
        data={articles}
        contentContainerStyle={{
          paddingVertical: StatusBar.currentHeight * 2.5,
        }}
        keyExtractor={item => item.key.toString()}
        renderItem={({item, index}) => (
          <Article {...item} id={index} navigation={navigation} />
        )}
      />
      {/* <Button
        title="goto"
        onPress={() => navigation.navigate('Notification')}
      /> */}
    </View>
  );
}
const styles = StyleSheet.create({
  text: {
    ...StyleSheet.absoluteFill,
    marginTop: StatusBar.currentHeight * 0.5,
    marginHorizontal: width * 0.03,
    fontFamily: 'Nunito-Bold',
    fontSize: 28,
  },
});
export default HomeScreen;
