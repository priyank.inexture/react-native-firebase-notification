/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  View,
  Text,
  BackHandler,
} from 'react-native';
import {SharedElement} from 'react-navigation-shared-element';
// import {useGlobalStateContext} from '../context/globalContext';
const {width, height} = Dimensions.get('window');
function DetailScreen(props) {
  // const { notification } = useGlobalStateContext();
  const {urlToImage, title, content, id} = props.navigation.state.params;
  const handleBackButtonClick = () => {
    props.navigation.navigate('Home');
    return true;
  };
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );
    };
  });
  return (
    <View>
      <View>
        <SharedElement id={`article.${id}.photo`}>
          <Image
            style={{
              // ...StyleSheet.absoluteFillObject,
              width: width,
              opacity: 1,
              height: height * 0.6,
              resizeMode: 'cover',
            }}
            source={{uri: urlToImage}}
          />
        </SharedElement>
      </View>
      <View
        style={{
          paddingHorizontal: width * 0.02,
          flexDirection: 'column',
        }}>
        <SharedElement id={`article.${id}.heading`}>
          <Text
            style={{
              marginVertical: height * 0.01,
              fontSize: 21,
              textAlign: 'left',
              fontFamily: 'nunitoBold',
            }}>
            {title}
          </Text>
        </SharedElement>
        <Text
          style={{
            marginTop: width * 0.06,
            fontSize: 16,
            textAlign: 'justify',
            fontFamily: 'nunitoRegular',
          }}>
          {content}
        </Text>
      </View>
    </View>
  );
}
DetailScreen.sharedElements = (navigation, otherNavigation, showing) => {
  const id = navigation.getParam('id');
  return [
    {
      id: `article.${id}.photo`,
      animation: 'fade',
      // resize: 'stretch'
    },
    {
      id: `article.${id}.heading`,
      animation: 'fade',
      resize: 'clip',
    },
  ];
};
export default DetailScreen;
