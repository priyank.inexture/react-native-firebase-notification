/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  TouchableWithoutFeedback,
  Dimensions,
  Image,
  StyleSheet,
  Text,
  Animated,
  View,
} from 'react-native';
const {width, height} = Dimensions.get('window');
import {SharedElement} from 'react-navigation-shared-element';
function Article(props) {
  const {urlToImage, title, id} = props;

  return (
    <TouchableWithoutFeedback
      onPress={() => props.navigation.navigate('Detail', {...props})}>
      <View style={styles.cardContainer}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
          }}>
          <SharedElement id={`article.${id}.photo`}>
            <Image
              source={{uri: urlToImage}}
              style={{
                // ...StyleSheet.absoluteFillObject,
                resizeMode: 'cover',
                width: width * 0.94,
                height: height * 0.4,
              }}
            />
          </SharedElement>
        </View>
        <View style={{paddingHorizontal: 0.03 * width}}>
          <SharedElement id={`article.${id}.heading`}>
            <Text
              style={{
                fontSize: 16,
                fontFamily: 'Nunito-Bold',
              }}>
              {title}
            </Text>
          </SharedElement>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}
const styles = StyleSheet.create({
  cardContainer: {
    marginVertical: height * 0.01,
    marginHorizontal: 0.03 * width,
    paddingBottom: 0.01 * height,
    overflow: 'hidden',
    borderRadius: 15,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
});
export default Article;
