import React, {createContext, useReducer, useContext} from 'react';
import {
  TEST_ACTION,
  ADD_NEW_ARTICLE_ACTION,
  UPDATE_EXPO_TOKEN_ACTION,
  UPDATE_NOTIFICATION,
} from './ActionType';
const ArticleData = require('../../assets/data.json');
const GlobalStateContext = createContext();
const GlobalDispatchContext = createContext();
const globalReducer = (state, action) => {
  switch (action.type) {
    case TEST_ACTION: {
      return {
        ...state,
        test: 2,
      };
    }
    case ADD_NEW_ARTICLE_ACTION: {
      try {
        const newArticle = {...action.article, key: state.articles.length + 1};
        let newArticles = [...state.articles];
        newArticles.push(newArticle);
        // console.log('new Artiicles', newArticles);
        return {
          ...state,
          articles: newArticles,
        };
      } catch (error) {
        console.log('Some error occured');
      }
      return {
        ...state,
      };
    }
    case UPDATE_EXPO_TOKEN_ACTION:
      return {
        ...state,
        expoPushToken: action.token ? action.token : '',
      };
    case UPDATE_NOTIFICATION:
      return {
        ...state,
        notification: action.notification ? action.notification : false,
      };
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};

//Provider
export const GlobalProvider = ({children}) => {
  const [state, dispatch] = useReducer(globalReducer, {
    test: 1,
    expoPushToken: '',
    notification: false,
    articles:
      ArticleData.articles.map((a, index) => ({...a, key: index})) || [],
  });

  return (
    <GlobalDispatchContext.Provider value={dispatch}>
      <GlobalStateContext.Provider value={state}>
        {children}
      </GlobalStateContext.Provider>
    </GlobalDispatchContext.Provider>
  );
};

//custom hooks for when we want to use our global state
export const useGlobalStateContext = () => useContext(GlobalStateContext);

export const useGlobalDispatchContext = () => useContext(GlobalDispatchContext);
