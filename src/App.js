/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';
import {
  Button,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import PushNotification from 'react-native-push-notification';
import {createSharedElementStackNavigator} from 'react-navigation-shared-element';
import {createAppContainer} from 'react-navigation';
import firebase from '@react-native-firebase/app';
import HomeScreen from './screens/HomeScreen';
import NotificationScreen from './screens/NotificationScreen';
import DetailScreen from './screens/DetailScreen';
import {fetchArticleDetail} from './util';
import {
  ADD_NEW_ARTICLE_ACTION,
  UPDATE_NOTIFICATION,
} from './context/ActionType';
import {useGlobalDispatchContext} from './context/globalContext';
const stackNavigator = createSharedElementStackNavigator(
  {
    Home: HomeScreen,
    Detail: DetailScreen,
    Notification: NotificationScreen,
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
    defaultNavigationOptions: {
      // headerBackTitleVisible: false,
      cardStyleInterpolator: ({current: {progress}}) => {
        return {
          cardStyle: {
            opacity: progress,
          },
        };
      },
    },
  },
);
const AppContainerNavigator = createAppContainer(stackNavigator);
const App = () => {
  const dispatch = useGlobalDispatchContext();
  useEffect(() => {
    if (firebase.app.length === 0) {
      firebase.initializeApp();
    }
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        console.log('TOKEN:', token);
      },

      // (required) Called when a remote is received or opened, or local notification is opened
      onNotification: function (notification) {
        console.log('NOTIFICATION:', notification);
        if (notification.data.id) {
          fetchArticleDetail(notification.data.id).then(data => {
            console.log('INSIDE PROMISE', data);
            const {body} = JSON.parse(data);
            dispatch({
              type: UPDATE_NOTIFICATION,
              notification: body,
            });
            dispatch({
              type: ADD_NEW_ARTICLE_ACTION,
              article: body || null,
            });
          });
        }
        // console.log('DISPATCG', ));
        // process the notification

        // (required) Called when a remote is received or opened, or local notification is opened
        // notification.finish(PushNotificationIOS.FetchResult.NoData);
      },

      // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
      onAction: function (notification) {
        console.log('ACTION:', notification.action);
        console.log('NOTIFICATION:', notification);

        // process the action
      },

      // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },

      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
       * (optional) default: true
       * - Specified if permissions (ios) and token (android and ios) will requested or not,
       * - if not, you must call PushNotificationsHandler.requestPermissions() later
       * - if you are not using remote notification or do not have Firebase installed, use this:
       *     requestPermissions: Platform.OS === 'ios'
       */
      requestPermissions: true,
    });
    PushNotification.subscribeToTopic('cat');
    // PushNotification.popInitialNotification(initialNotification => {
    //   console.log('Initial Notification', initialNotification);
    // });
  });
  return (
    <View style={styles.container}>
      <AppContainerNavigator />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
