const fetchArticleDetail = id => {
  return new Promise((resolve, _) => {
    resolve(
      '{"bigText":"New Article","subText":"Your favorite author posted a new article in a while!","message":"Your favorite author posted a new article in a while!","body":{"source":{"id":"the-wall-street-journal","name":"The Wall Street Journal"},"author":"Nina Trentmann","title":"Pinterest CFO Looks to Ramp Up Spending on Marketing, Product Investments","description":"The image-sharing platform expects the number of U.S. users to plateau as the pandemic abates","url":"https://www.wsj.com/articles/pinterest-cfo-looks-to-ramp-up-spending-on-marketing-product-investments-11619570245","urlToImage":"https://images.wsj.net/im-330866/social","publishedAt":"2021-04-28T00:41:49Z","content":"Pinterest Inc.s\\r\\nfinance chief plans to spend more money on marketing to offset a potential slowdown in activity on its image-sharing platform as the U.S. economy reopens.The San Francisco-based comp… [+2861 chars]"}}',
    );
  });
};
export {fetchArticleDetail};
